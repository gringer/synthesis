---
title: "Synthesis -- The Synthetic Board Game"
output:
  pdf_document: default
  html_document: default
---

We live in a world where proteins are all around us. Life is teeming
with possibilities, and we have become magical machines that can
manipulate chemistry and physics with atomic precision.

But what if you had nothing to work with? What if you had to start
from scratch? Do you have what it takes to build the world?

# Synopsis

Time: approx 60 mins

*Synthesis* is a game with the goal of creating the Spark of Life. The
world starts off almost completely empty, with a scattering of basic
chemicals. Chemicals can increase in complexity, and be combined
together to create peptides. Peptides can be combined together to
create proteins.

And proteins...? Proteins make Synthesis easier. Proteins give you
energy. Proteins are, quite literally, a game changer.

# Ingredients

You will need:

* A *Synthesis* game board made from a hexagonal grid. An A4-printable
  hexagonal hex board in two pieces can be found
  [here](synthesis_board.pdf).

* A set of protein cards. An example card set can be found
  [here](synthesis_cards.pdf).

* Chemical counters of increasing complexity (called 1,2,3,4), at
  least 10 of each

* Peptides of different shapes or colours (called 2,3,5,7), at least 6
  of each. Small coloured cubes are an appropriate size.

* Peptide linkers to mark extended peptide chains, at least ten per
  player. These should be small directional indicators (e.g. triangles
  or arrows) that can be placed on the edges of the hexes to indicate
  chain extension.

* Hexagonal reaction site markers labelled 1-6, one set for each
  player.

* Four reaction site spaces in front of each player.

* An energy (score) track with one counter per person (or pen and
  paper).

# Game Setup

1. Assemble the game board by joining the two sides together at their
longest edge

2. Drop three [**1**] chemicals and three [**2**] chemicals onto the
centre of the board from a height of 30cm, and place them in the
centre of the nearest hex. If they end up outside the board area,
place them on the nearest hex on the edge of the board.

3. Place the player counters on position 1 of the energy track --
everyone starts with 1 energy, use it responsibly!

4. Shuffle the protein cards and deal ***four*** out beside the game
board as the protein display.

5. Place one "Spark of life" card for each player beside the game
board.

4. The person who last ate soup starts the game.

# Game Play

Each player, in turn, carries out ***four*** of the following
actions. Passing is not permitted. The same action may be repeated
more than once:

* Place the lowest complexity chemical [**1**] in a blank space on the
  board.

* Increase the complexity of a chemical on the board.

* Move a chemical one hex space.

* Fuse a chemical with an adjacent chemical to create a valid peptide.

* Move a peptide one hex space. Peptides that are part of an existing
  protein chain cannot be moved.

* Create a reaction surface underneath an existing peptide, starting a
  protein chain.

* Extend a protein chain attached to one of your reaction surfaces
  with an adjacent peptide using a peptide linker.

* Break up a protein attached to one of your reaction surfaces (either
  complete or incomplete) into its component peptides.

* Use the power on one of your protein cards. See section "Proteins"
  below.

* Discard all cards in the protein display, and draw ***four*** new
  cards.
 
The game ends one round after the first person completes a "Spark of
Life" protein. The winner of the game is the person who has the most
energy at the end of the game.

# Proteins

*"What you must learn is that these rules are no different than the
 rules of a computer system... some of them can be bent. Others... can
 be broken."* - Morpheus, The Matrix

Proteins give *Synthesis* its life, and a complexity that increases as
the game progresses. The rules of the game can be changed by proteins.

Use proteins wisely: most proteins require one action to use, and will
be broken up by their use. In this case, a protein is broken up as in
the "Break up" action (see [Breaking up
proteins](#breaking-up-proteins)).

Some proteins are catalytic enzymes and are not broken up by use;
these are indicated by a **C** on the card. Some proteins do not use
up any action points when they are used; these are indicated by a
**0** on the card. Some proteins are *destroyed* by use, returning all
component peptides to the stock (not to the game board, as is usual)
and discarding the card; these are indicated by a **D** on the card.

Feel free to suggest other protein powers
[here](https://github.com/gringer/synthesis/issues), and they will
probably be added to the example card set. Please also report an issue
if a protein feels overpowered or underpowered; the design allows for
tweaks to be easily made to adjust how easy or hard it is to create a
protein.
 
# Notes

## Increasing complexity

The numerical value of chemicals is increased in a step-wise fashion
to the next highest numerical value (i.e. [**1**]->[**2**],
[**2**]->[**3**], [**3**]->[**4**]). Chemical complexity cannot be
increased beyond [**4**].

## Moving objects

Chemicals and peptides can be moved, but cannot occupy the same space
as other board objects (except when converting chemicals into
peptides). Reaction sites and protein chains cannot be moved.

## Converting chemicals into peptides

Convert chemicals on one hex space into a peptide by moving another
chemical to the same space and adding up the complexity of the
chemicals. The chemicals are replaced with the corresponding peptide
that matches their complexity sum. If the complexity of all chemicals
on a hex space does not add up to a valid peptide number (i.e. the sum
must match 2, 3, 5, or 7), then no peptide can be created.

## Creating reaction surfaces

Create a reaction surface by taking a reaction surface marker and
placing it underneath the starting peptide; this does *not* use any
energy. If there are no available reaction site spaces (each player
begins with ***four***), then you cannot create a new reaction
surface.

The starting peptide must match the starting peptide on one of the
protein cards in the protein display. Place that protein card onto one
of the reaction site spaces in front of you. Fill the protein display
back up to ***four*** cards (not including the "Spark of Life"
cards). If there are no available cards, shuffle the discard pile and
create a new protein stack.

## Extending proteins

Extend a peptide chain (protein) by attaching a peptide linker between
the chain end and an adjacent peptide. The next peptide *must* match
the next peptide on the protein card associated with the extended
protein. Reduce your energy track by the number of existing peptides
in the chain, i.e.:

* 1 existing peptide -- -1 power
* 2 existing peptides -- -2 power
* 3 existing peptides -- -3 power
* 4 existing peptides -- -4 power

If you do not have enough energy to extend the peptide chain, then it
cannot be extended.

Proteins must be extended in the order indicated on the reaction
surface card. As soon as a protein is complete (i.e. all the available
peptide positions are filled), power is added to the player's power
track according to the length of the protein. This recovers the
construction cost of the protein:

 * 2 peptides -- 1 power
 * 3 peptides -- 3 power
 * 4 peptides -- 6 power
 * 5 peptides -- 10 power
 
## Breaking up proteins
 
When a protein (either complete or incomplete) is broken up, peptide
linkers are removed, the reaction site marker is returned to the
player, and peptides are once again available for *all* players to
move. The protein card is placed in a discard pile next to the card
stack. Increase your energy track *by the number of removed linkers*.

The protein effect is *not* activated (unless a completed protein has
been otherwise activated).

# Winning *Synthesis* With a Spark of Life

Each person also has a single "Spark of Life" card. This card costs an
additional five (5) energy when creating a reaction surface, and may
be used instead of a protein card; it is placed on a reaction site
space in the same fashion as other protein cards.

*Synthesis* ends when anyone has a completed "Spark of Life" protein
at the *beginning* of their turn. Note that there are some proteins
that can break up or destroy completed proteins, so completing the
"Spark of Life" protein does not necessarily mean the game will end at
the beginning of the completing player's next turn.

The winner of *Synthesis* is the person with a completed "Spark of
Life" protein who has the highest energy level. In the case of an
energy level tie, the person who came last in the turn order is the
winner.