#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)
library(grid)

wrapper <- function(x, ...){ ## https://stackoverflow.com/a/3935429/3389895
  paste(strwrap(x, ...), collapse = "\n")
}

pointDist <- function(x1, y1, x2, y2){
    px1 <- x1  + (y1 %% 2) * 0.5;
    py1 <- y1 * sin(pi/3) * 1/3;
    px2 <- x2  + (y2 %% 2) * 0.5;
    py2 <- y2 * sin(pi/3) * 1/3;
    dist <- round(sqrt((px1 - px2)^2 + (py1 - py2)^2) / (sin(pi/3) * 2/3), 1);
    return(dist);
}

drawLinker <- function(ldf){
    if(nrow(ldf) == 0){
        return();
    }
    px1 <- ldf$x1 + (ldf$y1 %% 2) * 0.5;
    px2 <- ldf$x2 + (ldf$y2 %% 2) * 0.5;
    py1 <- ldf$y1 * sin(pi/3) * 1/3;
    py2 <- ldf$y2 * sin(pi/3) * 1/3;
    pcx <- (px1 + px2) / 2;
    pcy <- (py1 + py2) / 2;
    pca <- atan2(py1 - py2, px1 - px2);
    polygon(x=rep(pcx, each=5) +
                cos(c(0:3 + 0.5,NA) * 2/3 * pi +
                        rep(pca, each=5)) * 0.04 * c(2,3,2,2,1), 
            y=rep(pcy, each=5) +
                sin(c(0:3 + 0.5,NA) * 2/3 * pi +
                        rep(pca, each=5)) * 0.04 * c(2,3,2,2,1),
            col="yellow", border="black");
}

getFullName <- function(proteinName, loc, proteinChains, proteinCards){
  chain <- getChain(loc, proteinChains);
  pSeq <- 
    unlist(strsplit(proteinCards$sequence[match(proteinName, proteinCards$Name)],
                    "▻"));
  pSeq[1] <- paste0("{",pSeq[1]);
  pSeq[length(chain)] <- paste0(pSeq[length(chain)], "}");
  if(grepl("^spark-", proteinName)){
    proteinName <- "Spark of Life";
  }
  retVal <- sprintf("%s %s", proteinName, paste(pSeq, collapse="▻"));
  return(retVal);
}

isProteinComplete <- function(proteinName, loc, proteinChains, proteinCards){
  chain <- getChain(loc, proteinChains);
  pSeq <- 
    unlist(strsplit(proteinCards$sequence[match(proteinName, proteinCards$Name)],
                    "▻"));
  pSeq <- paste0("p", head(tail(pSeq, -length(chain)), 1));
  return(pSeq == "p");
}

getNextPeptide <- function(proteinName, loc, proteinChains, proteinCards){
  chain <- getChain(loc, proteinChains);
  pSeq <- 
    unlist(strsplit(proteinCards$sequence[match(proteinName, proteinCards$Name)],
                    "▻"));
  pSeq <- paste0("p", head(tail(pSeq, -length(chain)), 1));
  if(pSeq == "p"){
    pSeq <- "";
  }
  return(pSeq);
}

getChain <- function(loc, proteinChains){
    currentChain <- list(loc);
    lastPos <- loc;
    ## extend out to the end of the chain
    while(any((proteinChains[,"x1"] == lastPos[1]) &
              (proteinChains[,"y1"] == lastPos[2]))){
        chainPos <- which((proteinChains[,"x1"] == lastPos[1]) &
                              (proteinChains[,"y1"] == lastPos[2]));
        lastPos <- unlist(proteinChains[chainPos,c("x2","y2")]);
        currentChain <- c(currentChain, list(lastPos));
    }
    ## extend out to the start of the chain
    lastPos <- currentChain[[1]];
    while(any((proteinChains[,"x2"] == lastPos[1]) &
              (proteinChains[,"y2"] == lastPos[2]))){
        chainPos <- which((proteinChains[,"x2"] == lastPos[1]) &
                              (proteinChains[,"y2"] == lastPos[2]));
        lastPos <- unlist(proteinChains[chainPos,c("x1","y1")]);
        currentChain <- c(list(lastPos), currentChain);
    }
    return(currentChain);
}

chainLength <- function(loc, proteinChains){
    return(length(getChain(loc, proteinChains)));
}

confirmBreak <- function(loc, proteinChains){
    freedEnergy <- (chainLength(loc, proteinChains) - 1);
    modalDialog(title="Confirm Protein Break",
                span(sprintf("Break protein chain including the peptide at (%d,%d)?",
                             loc[1], loc[2])),
                span(sprintf("[This will free %d energy]", freedEnergy)),
                easyClose=TRUE,
                footer=tagList(modalButton("Cancel"),
                               actionButton("breakProtein", "Break Protein"))
                );
}

endGameDialog <- function(){
  modalDialog(title="Game End",
              "The game has ended; you completed the spark of life, and held on to it!",
              easyClose=FALSE,
              footer=tagList(actionButton("endGame", "End Game")));
}

loadProteins <- function(){
  primes <- c(2,3,5,7,11,13,17,19);
  proteinMetaData <- read.csv("../cardData.csv");
  proteinMetaData <- 
    proteinMetaData[!grepl("spark of life", 
                           proteinMetaData$Description),];
  proteinMetaData$sequence <- sapply(proteinMetaData$PeptideCount, function(cl){
    pep.seq <- sample(primes[1:4], cl, replace=TRUE);
    paste(pep.seq, collapse="▻");
  });
  proteinMetaData <- proteinMetaData[sample(nrow(proteinMetaData)),];
  return(proteinMetaData);
}

createSparks <- function(np){
  primes <- c(2,3,5,7,11,13,17,19);
  sparkLen <- 4;
  spark.df <- 
    data.frame(
      PeptideCount=sparkLen,
      Name=paste0("spark-", seq_len(np)),
      Description=paste0("The spark of life (requires 5 extra energy to start). ",
                         "If complete at the start of your next turn, the game ends."),
      Options="",
      sequence=replicate(np, paste(sample(primes[1:sparkLen], 
                                          sparkLen, replace=TRUE), 
                                   collapse="▻"))
      );
  return(spark.df);
}

chooseProtein <- function(loc, card.data){
  modalDialog(title="Choose Protein",
              tags$p(sprintf(paste0("More than one protein in the display starts ",
                                  "with the peptide at (%d,%d). Please choose one:"),
                           loc[1], loc[2])),
              tags$br(),
              radioButtons("proteinSelection", label = "Protein Name", 
                           choiceNames=sub("spark-.*$", "Spark of Life (-5 energy)",
                                           card.data$Name),
                           choiceValues=card.data$Name),
              easyClose=TRUE,
              footer=tagList(modalButton("Cancel"),
                             actionButton("selectProtein", "Select Protein"))
  );
}

drawCard <- function(card.data, done=0){
  card.power <- c(1,3,6,10);
  card.length <- card.data$PeptideCount;
  pep.str <- card.data$sequence;
  pep.seq <- as.numeric(unlist(strsplit(pep.str, "▻")));
  if(grepl("spark of life", card.data$Description)){
    pep.str <- paste0("(5) + ", pep.str);
  }
  grid.rect(width=1, height=1, gp=gpar(lwd=3));
  grid.text(pep.str, x=0.1, y=0.95, just="left");
  grid.text(card.data$Name, x=0.5, y=0.95, just="centre");
  grid.text(wrapper(card.data$Description, 40),
            x=0.1, y=0.85, just=c("left","top"));
  grid.text(sprintf("[%d]", card.power[card.length]),
            x=0.95, y=0.95, just="right");
  if(card.data$Options != ""){
    grid.text(paste(unlist(strsplit(card.data$Options,"")),
                    collapse="/"),
              x=0.95, y=0.05, just="right");
  }
  numRows <- trunc(card.length / 2 + 0.5);
  numCols <- ceiling(card.length / numRows);
  pushViewport(viewport(layout=grid.layout(nrow=numRows, ncol=numCols),
                        width=0.7, height=0.4, x=0.5, y=0.25));
  for(pnum in 0:(card.length-1)){
    lCol <- pnum%%numCols+1;
    lRow <- trunc(pnum/numCols) + 1;
    pushViewport(viewport(layout.pos.col=lCol,
                          layout.pos.row=lRow));
    pushViewport(viewport(width=unit(1,"snpc"), height=unit(1,"snpc"),
                          just=ifelse(lCol==1,"right","left"),
                          x=ifelse(lCol==1,1,0)));
    gridCol <- if(pnum < done) { "black" } else { "grey" };
    grid.rect(gp=gpar(lwd=3, col=gridCol));
    grid.text(pep.seq[pnum+1], gp=gpar(col=gridCol));
    popViewport(2);
  }
  popViewport(1);
}

# Define UI for application that draws a histogram
ui <- fluidPage(
    tags$head(tags$style('
    #hoverOverlay {
      position: relative;
      top: -600px;
      left: 0;
      height: 0;
      z-index: 100;
      padding: 0;
      margin: 0;
      margin-bottom: -600px;
    }
    #plotContainer {
      height: 600px;
    }
    ')),
    # Application title
    titlePanel("Synthesis"),

    # Sidebar with a slider input for number of bins 
    sidebarLayout(
        sidebarPanel(
            sliderInput("moveDist", label="Move Distance", min = 1,
                         max=5, value = 1),
            radioButtons("player", label="Player",
                         choices = c("1", "2", "3", "4", "5")),
            actionButton("useProtein", label="Use Protein")
        ),

        # Show a plot of the generated distribution
        mainPanel(
          tabsetPanel(
            id="gameTabs",
            tabPanel(
              "Board",
              fluidRow(
                tags$div(id="plotContainer",
                         plotOutput("boardPlot", height = 600),
                         plotOutput("hoverOverlay", height=600,
                                    hover = hoverOpts("grid_hover", delay=100,
                                                      delayType="throttle"),
                                    click = "grid_click"))),
              uiOutput("turnStats")
            ),
            tabPanel("Proteins",
                     plotOutput("proteinPlot", height=600))
          )
        )
    )
);

# Define server logic required to draw a histogram
server <- function(input, output, session) {
  values <- reactiveValues();
  values$gridWidth <- 15;
  values$hoverX <- -1;
  values$hoverY <- -1;
  values$clickedX <- -1;
  values$clickedY <- -1;
  values$moveDist <- 1;
  values$proteinChains <- data.frame(x1=numeric(), y1=numeric(),
                                     x2=numeric(), y2=numeric());
  values$linkedPoints <- matrix(FALSE, nrow = 30, ncol=20);
  values$boardPoints <- data.frame(x=numeric(), y=numeric(),
                                   gridX=numeric(), gridY=numeric());
  values$boardData <- matrix("", nrow = 30, ncol=20);
  values$boardOwnership <- matrix("", nrow = 30, ncol=20);
  values$boardProteins <- matrix("", nrow = 30, ncol=20);
  values$movesLeft <- 3;
  values$energy <- list("1" = 10, "2" = 10, "3" = 10, "4" = 10, "5" = 10);
  values$reactions <- list("1" = 4, "2" = 4, "3" = 4, "4" = 4, "5" = 4);
  values$sparkUsed <- list("1" = FALSE, "2" = FALSE, "3" = FALSE,
                            "4" = FALSE, "5" = FALSE);
  values$sparkComplete <- list("1" = FALSE, "2" = FALSE, "3" = FALSE,
                               "4" = FALSE, "5" = FALSE);
  sparks <- createSparks(5);
  proteins <- loadProteins();
  values$proteinCards <- rbind(proteins, sparks);
  values$proteinDraw <- proteins;
  values$proteinDiscard <- data.frame();
  values$proteinDisplay <- data.frame();
  values$sparkSequences <- sparks;

  finishTurn <- function(){
    values$movesLeft <- values$movesLeft - 1;
    if(values$movesLeft < 0){
      values$movesLeft <- 3;
      updateRadioButtons(session, "player",
                         selected = c("1" = "2",
                                      "2" = "3",
                                      "3" = "4",
                                      "4" = "5",
                                      "5" = "1")[input$player])
    }
  }
  
  ## Show statistics
  output$turnStats <- renderUI({
    tags$p(tags$strong("Moves left: "),
           values$movesLeft + 1,
           tags$br(),
           tags$p(tags$strong("Stack: "),
                  nrow(values$proteinDraw),
                  "; ",
                  tags$strong("Discard: "),
                  nrow(values$proteinDiscard)),
           tags$p(tags$strong("Energy: "),
                  values$energy[input$player],
                  "; ",
                  tags$strong("Reactions: "),
                  values$reactions[input$player]),
           if(values$clickedX != -1){
             pos <- c(values$clickedX, values$clickedY);
             posStr <- values$boardData[pos[2], pos[1]];
             tags$p(tags$strong("Click:"), 
                    sprintf("(%d, %d)", pos[1], pos[2]),
                    if(substr(posStr, 1, 1) == "p"){
                      if(values$boardProteins[pos[2], pos[1]] != ""){
                        tags$p(tags$strong("Protein chain:"), 
                               getFullName(values$boardProteins[pos[2], pos[1]],
                                           c(pos[1], pos[2]),
                                           values$proteinChains,
                                           values$proteinCards));
                      } else {
                        tags$p(tags$strong("Peptide:"), substring(posStr, 2));
                      }
                    } else if(substr(posStr, 1, 1) == "c"){
                      tags$p(tags$strong("Chemical:"), substring(posStr, 2));
                    })
           } else { "" }
    );
  });
  
  ## Show the base game board
  output$boardPlot <- renderPlot({
    values$gridWidth * 1.5 -> binsY;
    par(mar=c(0,0,0,0), bg=NA);
    boardPoints <- data.frame(x=numeric(), y=numeric(), 
                              gridX=numeric(), gridY=numeric());
    plot(NA, xlim=c(0, (values$gridWidth / 2) + 2), 
         ylim=c(0, binsY + 2) * sin(pi/3) * 1/3, ann=FALSE,
         axes=FALSE);
    seq(0, 2*pi, by=pi/3) -> angs;
    sy <- binsY; sx <- values$gridWidth;
    boardData <- values$boardData;
    boardOwnership <- values$boardOwnership;
    linkedPoints <- values$linkedPoints;
    for(y in seq_len(binsY)){
      for(x in seq_len((values$gridWidth+((y+1) %% 2)) / 2)){
        px <- x  + (y %% 2) * 0.5;
        py <- y * sin(pi/3) * 1/3;
        plw <- if(linkedPoints[y, x]) { 4 } else { 2 };
        points(px + cos(angs) * 1/3,
               py + sin(angs) * 1/3, type="l", lwd=plw);
        boardPoints[nrow(boardPoints)+1,] <- c(px, py, x, y);
        if(boardOwnership[y, x] != ""){
          selectColour <- c(
            "1" = "#80300040",
            "2" = "#40009040",
            "3" = "#A000A040",
            "4" = "#0040A040",
            "5" = "#30700040")[boardOwnership[y, x]];
          polygon(px + cos(angs) * 1/3,
                  py + sin(angs) * 1/3,
                  col=selectColour);
        }
        if(boardData[y,x] != ""){
          boardText <- boardData[y,x];
          boardType <- substring(boardText, 1, 1);
          boardNum <- as.numeric(substring(boardText, 2));
          if(boardType == "c"){
            text(px, py, boardNum, cex = 2.5, col="#0080A0");
          } else if(boardType == "p") {
            text(px, py, boardNum, cex = 2.5, col="#A00080");
          }
        }
      }
    }
    drawLinker(values$proteinChains);
    values$boardPoints <- boardPoints;
    values$boardData <- boardData;
  });
  
  ## Show hover overlay
  output$hoverOverlay <- renderPlot({
    values$gridWidth * 1.5 -> binsY;
    par(mar=c(0,0,0,0), bg=NA);
    plot(NA, xlim=c(0, (values$gridWidth / 2) + 2), 
         ylim=c(0, binsY + 2) * sin(pi/3) * 1/3, ann=FALSE,
         axes=FALSE);
    hx <- values$hoverX;
    hy <- values$hoverY;
    cx <- values$clickedX;
    cy <- values$clickedY;
    selectColour <- c(
      "1" = "#80300080",
      "2" = "#40009080",
      "3" = "#A000A080",
      "4" = "#0040A080",
      "5" = "#30700080")[input$player];
    if(cx != -1){
      pcx <- cx  + (cy %% 2) * 0.5;
      pcy <- cy * sin(pi/3) * 1/3;
      seq(0, 2*pi, by=pi/3) -> angs;
      points(pcx + cos(angs) * 1/3, pcy + sin(angs) * 1/3, 
             type="l", lwd = 9, col=selectColour);
    }
    if(hx != -1){
      phx <- hx  + (hy %% 2) * 0.5;
      phy <- hy * sin(pi/3) * 1/3;
      seq(0, 2*pi, by=pi/3) -> angs;
      points(phx + cos(angs) * 1/3, phy + sin(angs) * 1/3, 
             type="l", lwd = 5, col=selectColour);
    }
  });
  
  ## Show protein display
  output$proteinPlot <- renderPlot({
    grid.newpage();
    proteinCount <- 4;
    pushViewport(viewport(layout=grid.layout(nrow=ceiling((proteinCount + 1)/2), 
                                             ncol=2), width=0.95, height=0.95));
    for(cNum in 1:4){
      if(nrow(values$proteinDisplay) >= cNum){
        pushViewport(viewport(layout.pos.col=(cNum+1) %% 2+1,
                              layout.pos.row=trunc((cNum-1) / 2) + 1));
        drawCard(values$proteinDisplay[cNum,,drop=FALSE]);
        popViewport(1);
      }
    }
    lc <- 5;
    if(!values$sparkUsed[[input$player]]){
      ## Draw spark of life card
      pushViewport(viewport(layout.pos.col=(lc+1) %% 2+1,
                            layout.pos.row=trunc((lc-1) / 2) + 1));
      drawCard(values$sparkSequences[input$player,,drop=FALSE]);
      popViewport(1);
    }
  });
  
  ## Moving the mouse
  observeEvent(input$grid_hover, {
    gh <- input$grid_hover;
    if(!is.null(gh) && !is.null(values$boardPoints)){
      hoverPoints <- 
        nearPoints(values$boardPoints, gh, "x", "y",
                   threshold=Inf, maxpoints=1);
      if(nrow(hoverPoints) > 0){
        values$hoverX <- hoverPoints[1,"gridX"];
        values$hoverY <- hoverPoints[1,"gridY"];
      }
    }
  });
  
  ## Clicking the mouse
  observeEvent(input$grid_click, {
    moveDone <- FALSE;
    gc <- input$grid_click;
    if(!is.null(gc) && !is.null(values$boardPoints)){
      clickPoints <- 
        nearPoints(values$boardPoints, gc, "x", "y",
                   threshold=Inf, maxpoints=1);
      if(nrow(clickPoints) > 0){
        gridX <- clickPoints[1,"gridX"];
        gridY <- clickPoints[1,"gridY"];
        boardText <- values$boardData[gridY,gridX];
        boardType <- "c";
        boardNum <- 0;
        if(boardText != ""){
          boardType <- substring(boardText, 1, 1);
          boardNum <- as.numeric(substring(boardText, 2));
        }
        if(values$clickedX == -1){
          values$clickedX <- gridX;
          values$clickedY <- gridY;
        } else if(values$clickedX != -1){
          clickedX <- values$clickedX;
          clickedY <- values$clickedY;
          pd <- pointDist(gridX, gridY, clickedX, clickedY);
          boardOwnership <- values$boardOwnership;
          if((pd > 0) && (pd <= values$moveDist)){
            clickedText <- values$boardData[clickedY,clickedX];
            if(boardText == "" && clickedText != ""){
              ## moving
              if(boardOwnership[clickedY, clickedX] == ""){
                values$boardData[gridY, gridX] <-
                  values$boardData[clickedY, clickedX];
                values$boardData[clickedY, clickedX] <- "";
                moveDone <- TRUE;
              }
            } else if((boardType == "c") && (clickedText != "")) {
              ## Merging chemicals
              clickedType <- substring(clickedText, 1, 1);
              clickedNum <- as.numeric(substring(clickedText, 2));
              if((clickedType == "c") && 
                 ((clickedNum + boardNum) %in% c(2,3,5,7))){
                values$boardData[gridY, gridX] <-
                  paste0("p", clickedNum + boardNum);
                values$boardData[clickedY, clickedX] <- "";
                moveDone <- TRUE;
              }
            } else if(boardType == "p"){
              clickedType <- substring(clickedText, 1, 1);
              if(clickedType == "p") {
                if((boardOwnership[gridY, gridX] == input$player) && 
                   (boardOwnership[clickedY, clickedX] == "")){
                  ## Extending protein chain
                  energyNeeded <- 
                    chainLength(c(gridX, gridY), values$proteinChains);
                  if((pd == 1) && !values$linkedPoints[gridY, gridX] &&
                     (values$energy[[input$player]] >= energyNeeded)){
                    proteinChain <- getChain(c(gridX, gridY), values$proteinChains);
                    pPos <- proteinChain[[1]];
                    proteinName <- values$boardProteins[pPos[2], pPos[1]];
                    nextPep <- getNextPeptide(proteinName, pPos, 
                                              values$proteinChains,
                                              values$proteinCards);
                    if(nextPep == values$boardData[clickedY, clickedX]){
                      values$energy[[input$player]] <-
                        values$energy[[input$player]] - energyNeeded;
                      values$proteinChains <-
                        rbind(values$proteinChains,
                              data.frame("x1"=gridX, "y1"=gridY,
                                         "x2"=clickedX, "y2"=clickedY));
                      boardOwnership[clickedY, clickedX] <- 
                        input$player;
                      if(isProteinComplete(proteinName, pPos, 
                                           values$proteinChains,
                                           values$proteinCards)){
                        chainLen <- length(proteinChain);
                        energyToAdd <- (chainLen ^ 2 + chainLen) / 2;
                        values$energy[[input$player]] <-
                          values$energy[[input$player]] + energyToAdd;
                        if(grepl("^spark-", proteinName)){
                          values$sparkComplete[[input$player]] <- TRUE;
                          showModal(modalDialog(
                            title = "Completed Spark of Life",
                            "You've completed the spark of life! Keep that spark going ",
                            "until the beginning of your next turn to end the game."));
                        } else {
                          showModal(modalDialog(
                            title = "Complete protein",
                            sprintf("Protein chain '%s' is now complete; you get %d energy!",
                                    proteinName, energyToAdd)));
                        }
                      }
                      values$linkedPoints[gridY, gridX] <- TRUE;
                      values$boardOwnership <- boardOwnership;
                      moveDone <- TRUE;
                    } else {
                      if(nextPep == ""){
                        showModal(modalDialog(
                          title = "Invalid Protein Link",
                          sprintf("Error: protein chain '%s' is already complete!",
                                  proteinName)));
                      } else {
                        showModal(modalDialog(
                          title = "Invalid Protein Link",
                          sprintf("Error: next peptide for '%s' should be '%s', ",
                                  proteinName, nextPep),
                          sprintf("not '%s'", values$boardData[clickedY, clickedX])));
                      }
                    }
                  }
                } else if(boardOwnership[gridY, gridX] != input$player) {
                  showModal(modalDialog(
                    title = "Not Your Protein",
                    "Error: the current player does not own the protein ",
                    "on the clicked spot, so cannot extend it with another ",
                    "peptide."));
                }
              }
            }
          } else if(pd == 0){
            if((boardType == "c") && (boardNum < 4)){
              values$boardData[gridY, gridX] <- 
                paste0("c", boardNum + 1);
              moveDone <- TRUE;
            }
            if(boardType == "p"){
              ## starting a protein chain
              if(values$boardOwnership[gridY, gridX] == ""){
                if(values$reactions[[input$player]] >= 1){
                  ## check to make sure protein is valid
                  availableProteins <- 
                    if((values$energy[[input$player]] >= 5) &
                       (!values$sparkUsed[[input$player]])){
                      rbind(values$proteinDisplay,
                            values$sparkSequences[input$player,,
                                                  drop=FALSE]);
                    } else {
                      values$proteinDisplay;
                    }
                  proteinOptions <- 
                    paste0("p",
                           substring(availableProteins$sequence,
                                     1,1));
                  selectPoss <- which(proteinOptions %in% 
                                        values$boardData[gridY,gridX]);
                  enoughEnergy <- TRUE;
                  if(length(selectPoss) == 0){
                    showModal(modalDialog(
                      title = "Not a Known Protein",
                      "Cannot start a protein chain! ",
                      "No proteins in the display start with ",
                      "this peptide."));
                  } else if(length(selectPoss) == 1){
                    ## Setting ownership
                    proteinDisplay <- 
                      availableProteins[selectPoss,,drop=FALSE];
                    if(grepl("^spark-", proteinDisplay$Name)){
                      values$energy[[input$player]] <-
                        values$energy[[input$player]] - 5;
                      values$sparkUsed[[input$player]] <- TRUE;
                    } else {
                      if(any(grepl("^spark-", availableProteins$Name))){
                        ## Don't let the sparks get into the display pile
                        availableProteins <- 
                          availableProteins[!grepl("^spark-", availableProteins$Name),,
                                            drop=FALSE];
                      }
                      values$proteinDisplay <- 
                        availableProteins[-selectPoss,,drop=FALSE];
                    }
                    values$reactions[[input$player]] <-
                      values$reactions[[input$player]] - 1;
                    values$boardOwnership[gridY, gridX] <-
                      input$player;
                    values$boardProteins[gridY, gridX] <-
                      proteinDisplay$Name;
                    moveDone <- TRUE;
                  } else if(length(selectPoss) > 1) {
                    pos <- c(gridX, gridY);
                    values$posToModify <- pos;
                    showModal(chooseProtein(pos,
                      availableProteins[selectPoss,,drop=FALSE]));
                  }
                }
              } else if(values$boardOwnership[gridY, gridX] == 
                        input$player){
                pos <- c(gridX, gridY);
                values$posToModify <- pos;
                showModal(confirmBreak(pos, values$proteinChains));
              }
            }
          }
          values$clickedX <- -1;
          values$clickedY <- -1;
        }
      }
    }
    if(moveDone){
      finishTurn();
    }
  });
  ## Changing action type
  observeEvent(input$player, {
    values$hoverX <- -1;
    values$hoverY <- -1;
    values$clickedX <- -1;
    values$clickedY <- -1;
    values$movesLeft <- 3;
    if(nrow(values$proteinDisplay) < 4){
      numToAdd <- 4 - nrow(values$proteinDisplay);
      if(nrow(values$proteinDraw) < numToAdd){
        ## Shuffle discard pile, and put at bottom
        values$proteinDiscard <- 
          values$proteinDiscard[sample(nrow(values$proteinDiscard)),];
        values$proteinDraw <- rbind(values$proteinDraw, values$proteinDiscard);
        values$proteinDiscard <- data.frame();
      }
      if(nrow(values$proteinDraw) >= numToAdd){
        addedCards <- head(values$proteinDraw, numToAdd);
        cat("Adding", paste(addedCards$Name, collapse="; "), "\n");
        values$proteinDisplay <- rbind(values$proteinDisplay, addedCards);
        values$proteinDraw <- tail(values$proteinDraw, -numToAdd);
      } else {
        cat("Protein stack is not full enough\n");
      }
    }
    ## check for game end state
    if(values$sparkComplete[[input$player]] && values$sparkUsed[[input$player]]){
      showModal(endGameDialog());
    } else {
      values$sparkComplete[[input$player]] <- FALSE;
    }
  });
  ## Changing move distance
  observeEvent(input$moveDist, {
    values$moveDist <- input$moveDist;
  });
  ## Clicking on "Use Protein" button
  observeEvent(input$useProtein, {
    print(values$proteinDisplay);
    
  });
  ## Clicking 'Select Protein' in a protein modal dialog
  observeEvent(input$selectProtein, {
    changePos <- values$posToModify;
    gridX <- changePos[1]; gridY <- changePos[2];
    selectedProtein <- input$proteinSelection;
    ## Setting ownership
    selectPoss <- head(which(values$proteinDisplay$Name == selectedProtein), 1);
    if(grepl("^spark-", selectedProtein)) {
      values$energy[[input$player]] <-
        values$energy[[input$player]] - 5;
      values$sparkUsed[[input$player]] <- TRUE;
    } else {
      values$proteinDisplay <- 
        values$proteinDisplay[-selectPoss,,drop=FALSE];
    }
    values$reactions[[input$player]] <-
      values$reactions[[input$player]] - 1;
    values$boardOwnership[gridY, gridX] <-
      input$player;
    values$boardProteins[gridY, gridX] <-
      selectedProtein;
    finishTurn();
    removeModal();
  });

  ## Clicking 'Break Protein' in a protein modal dialog
  observeEvent(input$breakProtein, {
    proteinChains <- values$proteinChains;
    removePos <- values$posToModify
    removeChain <- getChain(removePos, proteinChains);
    firstPos <- unlist(removeChain[[1]]);
    owner <- values$boardOwnership[firstPos[2], firstPos[1]];
    proteinName <- values$boardProteins[firstPos[2], firstPos[1]];
    removeLines <- NULL;
    for(posL in removeChain){
      pos <- unlist(posL);
      values$boardOwnership[pos[2], pos[1]] <- "";
      values$linkedPoints[pos[2], pos[1]] <- FALSE;
      removeLines <- 
        c(removeLines, 
          which((proteinChains[,"x1"] == pos[1]) &
                  (proteinChains[,"y1"] == pos[2])));
    }
    if(length(removeLines) > 0){
      proteinChains <- proteinChains[-removeLines,];
    }
    if(!grepl("^spark-", proteinName)){
      ## Discard Protein to Discard Pile
      values$proteinDiscard <- 
        rbind(values$proteinDiscard,
              values$proteinCards[match(proteinName, 
                                      values$proteinCards$Name),]);
    } else {
      values$sparkUsed[[input$player]] <- FALSE;
      values$sparkComplete[[input$player]] <- FALSE;
    }
    ## Recover player energy and reactions
    values$energy[[owner]] <- values$energy[[owner]] + length(removeChain) - 1;
    values$reactions[[owner]] <- values$reactions[[owner]] + 1;
    values$proteinChains <- proteinChains;
    finishTurn();
    removeModal();
  });

  ## Clicking 'End Game'
  observeEvent(input$endGame, {
    stopApp();
  });
}
# Run the application 
shinyApp(ui = ui, server = server)
