extends Panel

var createMode = true

@onready var selectModeLabel = $Label_SelectMode

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func getCreateMode():
	return(createMode)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass


func _on_check_button_select_mode_toggled(toggled_on):
	createMode = toggled_on
	selectModeLabel.text = "Create" if createMode else "Destroy"
