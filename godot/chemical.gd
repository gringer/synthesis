class_name Chemical extends Sprite2D

const MAX_CHEMICAL_SIZE = 9
const VALID_PEPTIDE_SIZES = [2, 3, 5, 7, 11, 13, 17, 19, 23, 24, 25, 26, 27, 28, 29]

static var chemFont: Font = load("res://Pointilised-Bold-1.1.0.ttf")
static var peptideTexture = load("res://sphere_blob.svg")

var chemType = ""
var chemSize = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func setType(newType: String):
	chemType = newType

func getType():
	return(chemType)

func getSize():
	return(chemSize)

func setSize(newSize: int):
	chemSize = newSize

func increaseSize():
	if(chemType != "chemical"):
		return
	if(chemSize < MAX_CHEMICAL_SIZE):
		chemSize = chemSize + 1
		queue_redraw()

func combineWith(oldObj: Chemical):
	var oldType = oldObj.getType()
	var oldVal = oldObj.getSize()
	if((oldType == "chemical") && (chemType == "chemical") &&
	   ((oldVal + chemSize) in VALID_PEPTIDE_SIZES)):
		self.remove_child(oldObj)
		setSize(oldVal + chemSize)
		setType("peptide")
		texture = peptideTexture
		return true
	return false

func _draw():
	# Four circles for the 2 eyes: 2 white, 2 grey.
	if(chemSize > 0):
		draw_string(chemFont, Vector2(-45, 40), str(chemSize),
					HORIZONTAL_ALIGNMENT_CENTER, 90, 100)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
