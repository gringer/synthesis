extends TileMap

const TILE_BASE = 0
const TILE_OVERLAY = 1
const TILE_W = 2
const TILE_NW = 3
const TILE_NE = 4
const TILE_E = 5
const TILE_SE = 6
const TILE_SW = 7
const TILE_ENTROPY = 8

const BASE_HIGHLIGHT = Vector2i(2, 0)
const ENTROPY_EDGE = Vector2i(3, 0)
const ENTROPY_MAIN = Vector2i(4, 0)
const WALL_W = Vector2i(0, 1)
const WALL_NW = Vector2i(1, 1)
const WALL_NE = Vector2i(2, 1)
const WALL_E = Vector2i(3, 1)
const WALL_SE = Vector2i(4, 1)
const WALL_SW = Vector2i(5, 1)
const DC_THRESHOLD = 0.5

const TILE_INVALID = Vector2i(-999999, -999999)

var lastMousePos = Vector2i(0,0)
var animationTime = 0.0
var animationLength = 2.0
var sigma = 0.01
var is_clicked = false
var dblClickTime = 0.0
var lastAnimChemical: Chemical = null
var lastClickedChemical: Chemical = null
var lastChemicalIncreased = false
var lastTilePos = TILE_INVALID
var selectedChemical: Chemical = null

@onready var crystalTexture = load("res://crystal_horiz.svg")
@onready var reactionTexture = load("res://rubble.svg")
@onready var root = self.get_parent()
@onready var blastShader = preload("res://focusedBlast.gdshader")
@onready var panelOptions = root.get_node("Camera2D/Panel_SynthesisPanel")

func placeReactionSites():
	var activeTiles: Array = []
	for tilePos in self.get_used_cells(TILE_BASE):
		if(!((self.get_cell_atlas_coords(TILE_ENTROPY, tilePos) == ENTROPY_MAIN) ||
			 (self.get_cell_atlas_coords(TILE_ENTROPY, tilePos) == ENTROPY_EDGE))):
			activeTiles.push_back(tilePos)
	activeTiles.shuffle()
	var reactionSitePos = activeTiles.pop_back()
	var newSite: ReactionSite = ReactionSite.new()
	newSite.global_position = self.to_global(self.map_to_local(reactionSitePos))
	newSite.texture = reactionTexture
	newSite.scale = Vector2(0.2,0.2)
	newSite.modulate.a = 0.2
	self.add_child(newSite)

# Called when the node enters the scene tree for the first time.
func _ready():
	RenderingServer.global_shader_parameter_set("animation_time", animationTime)
	## remove existing debug objects
	for childObj in self.get_children():
		print("Found " + childObj.get_class())
		if((childObj is Chemical) || (childObj is ReactionSite)):
				self.remove_child(childObj)
	placeReactionSites()

func oddrToAxial(pos: Vector2i):
	## see [https://www.redblobgames.com/grids/hexagons/#conversions-offset]
	var q = pos.x - int((pos.y - (pos.y & 1)) >> 1)
	var r = pos.y
	return Vector2i(q, r)

func axialDistance(posA: Vector2, posB: Vector2):
	## see [https://www.redblobgames.com/grids/hexagons/#distances-axial]
	return((abs(posA.x - posB.x) +
			abs(posA.x + posA.y - posB.x - posB.y) +
			abs(posA.y - posB.y)) / 2)

func getTileDist(posA: Vector2i, posB: Vector2i):
	## According to [https://www.redblobgames.com/grids/hexagons/#distances],
	## godot uses "odd-r" notation for pointy hex grids
	var ac = oddrToAxial(posA)
	var bc = oddrToAxial(posB)
	return axialDistance(ac, bc)

func moveChemical(oldObj: Chemical, oldPos: Vector2, newPos: Vector2):
	var	newGlobalPos = self.to_global(self.map_to_local(newPos))
	if(getTileDist(oldPos, newPos) == 1):
		oldObj.global_position = newGlobalPos

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Set mouse cursor to mouse hover
	var tilePos = self.local_to_map(self.get_local_mouse_position())
	if(tilePos != lastMousePos):
		self.set_cell(TILE_W, lastMousePos, -1)
		self.set_cell(TILE_NE, lastMousePos, -1)
		self.set_cell(TILE_SE, lastMousePos, -1)
		self.set_cell(TILE_W, tilePos, 0, WALL_W)
		self.set_cell(TILE_NE, tilePos, 0, WALL_NE)
		self.set_cell(TILE_SE, tilePos, 0, WALL_SE)
		lastMousePos = tilePos
		is_clicked = false
		dblClickTime = 0.0
	else:
		dblClickTime += delta
	# Update animation time
	var hex_on_cell = false
	var entropy_cell = self.get_cell_atlas_coords(TILE_ENTROPY, tilePos) == ENTROPY_MAIN
	if (Input.is_action_just_pressed("deactivate") ||
		(Input.is_action_just_pressed("activate_hex") && entropy_cell)):
		lastClickedChemical = null
		self.clear_layer(TILE_OVERLAY)
		dblClickTime = 0.0
	if (Input.is_action_just_released("activate_hex")):
		var clickPos = self.to_global(self.map_to_local(tilePos))
		var newChemical: Chemical = null
		for childObj in self.get_children():
			if(childObj is Chemical):
				if(clickPos.distance_to(childObj.global_position) < 1):
					newChemical = childObj
		if(lastTilePos != tilePos):
			self.clear_layer(TILE_OVERLAY)
			self.set_cell(TILE_OVERLAY, tilePos, 0, BASE_HIGHLIGHT)
			if((lastClickedChemical != null) && (newChemical != null)):
				if(newChemical.combineWith(lastClickedChemical)):
					self.remove_child(lastClickedChemical)
			elif((lastClickedChemical != null) && (newChemical == null)):
				moveChemical(lastClickedChemical, lastTilePos, tilePos)
		pass
	if ((!entropy_cell) && (Input.is_action_just_pressed("activate_hex"))):
		var clickPos = self.to_global(self.map_to_local(tilePos))
		for childObj in self.get_children():
			if(childObj is Chemical):
				if(clickPos.distance_to(childObj.global_position) < 1):
					lastClickedChemical = childObj
					hex_on_cell = true
		if (!is_clicked || (dblClickTime > DC_THRESHOLD)):
			self.clear_layer(TILE_OVERLAY)
			is_clicked = true
			lastTilePos = tilePos
			self.set_cell(TILE_OVERLAY, tilePos, 0, BASE_HIGHLIGHT)
			dblClickTime = 0.0
		elif(is_clicked && (dblClickTime < DC_THRESHOLD)):
			if(!panelOptions.getCreateMode()):
				for childObj in self.get_children():
					if(childObj is Chemical):
						if(clickPos.distance_to(childObj.global_position) < 1):
							self.remove_child(childObj)
				return
			self.clear_layer(TILE_OVERLAY)
			for childObj in self.get_children():
				if(childObj is Chemical):
					if(clickPos.distance_to(childObj.global_position) < 1):
						childObj.increaseSize()
			if(!hex_on_cell):
				animationTime = 0.0
				var newChemical: Chemical = Chemical.new()
				newChemical.global_position = clickPos
				newChemical.texture = crystalTexture
				newChemical.scale = Vector2(0.2,0.2)
				var shm: ShaderMaterial = ShaderMaterial.new()
				shm.shader = blastShader
				shm.set_shader_parameter("point_count", 6)
				shm.set_shader_parameter("wave_length", 0.15)
				shm.set_shader_parameter("focal_radius", 0.31)
				shm.set_shader_parameter("starting_offset", -0.25)
				shm.set_shader_parameter("cycle_period", 2.0)
				newChemical.material = shm
				newChemical.setType("chemical")
				self.add_child(newChemical)
				if(lastAnimChemical != null):
					if(!lastChemicalIncreased):
						lastAnimChemical.increaseSize()
					lastAnimChemical.material = null
				lastChemicalIncreased = false
				lastAnimChemical = newChemical
	else:
		animationTime = animationTime + delta
		if((animationTime >= (animationLength / 2.0)) && lastAnimChemical != null):
			if(!lastChemicalIncreased):
				lastAnimChemical.increaseSize()
				lastChemicalIncreased = true
		if((animationTime >= (animationLength - sigma)) && lastAnimChemical != null):
			lastAnimChemical.material = null
			lastAnimChemical = null

	# Update animation frame
	RenderingServer.global_shader_parameter_set("animation_time", animationTime)
